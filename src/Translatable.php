<?php

namespace Enmaboya\Translatable;

use Laravel\Nova\Fields\Field;

class Translatable extends Field
{
    public $component = 'translatable';

    public function __construct($name, $attribute = null, $resolveCallback = null)
    {
        parent::__construct($name, $attribute, $resolveCallback);

        $locales = array_map(function ($value) {
            return __($value);
        }, config('translatable.locales'));

        $this->withMeta([
            'locales' => $locales,
            'indexLocale' => app()->getLocale()
        ]);
    }

    protected function resolveAttribute($resource, $attribute)
    {
        if (method_exists($resource, 'getTranslations')) {
            return $resource->getTranslations($attribute);
        }
        return data_get($resource, $attribute);
    }

    public function locales(array $locales)
    {
        return $this->withMeta(['locales' => $locales]);
    }

    public function indexLocale($locale)
    {
        return $this->withMeta(['indexLocale' => $locale]);
    }

    public function singleLine()
    {
        return $this->withMeta(['singleLine' => true]);
    }

    public function trix()
    {
        return $this->withMeta(['trix' => true]);
    }

    public function height($value)
    {
        return $this->withMeta(['height' => $value]);
    }
}
